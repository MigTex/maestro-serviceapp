# How to use:
![android_usage](service-app.gif)
```
 maestro test --format junit <flow.yaml>
 maestro studio
```
![cloud](cloud.gif)

# Pre requisites

### Java > 8 (use [jenv](https://www.jenv.be/))

### [Android CmdLine Tools](https://developer.android.com/studio/command-line) 

 The following steps where required to setup a AVD (Android Virtual Device) in an Ubuntu machine, but please take heed to the official documentation and your version of choice
```
unzip <package> 
mkdir ~/android_sdk/cmdline-tools/latest
mkdir ~/android_sdk/avd
mv <package> $HOME/android_sdk/cmdline-tools/latest
```

 Set env vars on your profile of choice
 
 (I've set it up under Home but docs advise under /opt)
```
# Android stuff
export ANDROID_HOME=$HOME/android_sdk
export ANDROID_USER_HOME=$HOME/android_sdk
export PATH=$PATH:$ANDROID_HOME/cmdline-tools/latest/bin
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
export PATH=$PATH:$ANDROID_HOME/emulator
export ANDROID_AVD_HOME=$ANDROID_HOME/avd/

Or better:

export ANDROID_HOME=$HOME/android_sdk
export ANDROID_USER_HOME=$HOME/android_sdk
export PATH=$PATH:$ANDROID_HOME/cmdline-tools/latest/bin:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools:$ANDROID_HOME/emulator
export ANDROID_AVD_HOME=$ANDROID_HOME/avd/


```

 Install tools
 ```
 sdkmanager "platform-tools" "emulator" "platforms;android-33" "build-tools;33.0.2" "system-images;android-33;google_apis;x86_64"
 ```

 Ensure of Device

 ```
avdmanager list device
 ```


 Create avd
 ```
 avdmanager create avd —name android33 —package “system-images;android-33;google_apis;x86_64” --device "Nexus One"
 ```
 Check your avd 

 ```
  emulator -list-avd
 ```
Start your emulator 

```
emulator @android33

```

Install your apk

```
adb devices 
# List of devices attached
# emulator-5554   device

adb -s emulator-5554 install Mercedes\ me\ Service_1.14.0_Apkpure.apk 

```
Uninstall the app (used tab)
```
adb uninstall com.daimler.ris.service.ece.android
```

### iOS (skipped due to hardware dependency)
Needs [Xcode](https://developer.apple.com/xcode/) tools installed.
Should have different app name and ci image differences but due to lack 
of hardware, it was not able to validate it.

### Maestro
```
 # Follow the steps for installation on 
 https://maestro.mobile.dev/getting-started/installing-maestro#connecting-to-your-device

 # Remember that maestro assumes the app is already installed in the device

```

